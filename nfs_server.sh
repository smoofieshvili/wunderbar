DIR="/kubenfs"
CLIENT_IP=192.168.0.101

yum install nfs-utils
mkdir -p $DIR
chmod -R 755 $DIR
chown nfsnobody:nfsnobody $DIR
systemctl enable rpcbind
systemctl enable nfs-server
systemctl enable nfs-lock
systemctl enable nfs-idmap
systemctl start rpcbind
systemctl start nfs-server
systemctl start nfs-lock
systemctl start nfs-idmap

echo "$DIR    $CLIENT_IP(rw,sync,no_root_squash,no_all_squash)" >> /etc/exports
systemctl restart nfs-server
firewall-cmd --permanent --zone=public --add-service=nfs
firewall-cmd --permanent --zone=public --add-service=mountd
firewall-cmd --permanent --zone=public --add-service=rpc-bind
firewall-cmd --reload
